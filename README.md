# README #

### What is this repository for? ###

This repository hosts a small Fortran program to assign valences on a periodic lattice such that the electro-static interaction energy (Ewald sum) is minimal. 

### How do I get set up? ###

After pulling the repository, you find a simple makefile to compile the program. You need a Fortran compiler available.

### Who do I talk to? ###

* Repo owner or admin
